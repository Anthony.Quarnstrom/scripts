
helmPurgeHome="C:/Users/gz70cm/code/DAPI/tools/helm-purge"
devBranch="release/dev"
qaBranch="release/qa"
cap1Branch="release/cap1"

branchRoute=""

function newfeature()
{
    echo "Creating new feature branch for $1 from master"
    git checkout master
    git add .
    git stash -m "Autostash"
    git pull
    git checkout -b feature/$1
    git push
    git checkout -b release/$1
    git push
    git checkout feature/$1
}

function releaseFeature()
{
    gitBranch=`git rev-parse --abbrev-ref HEAD`
    branchName=`echo $gitBranch | cut -d "/" -f 2`
    if [[ "$gitBranch" == *"feature"* ]]; then
        echo "Cleaning up feature in OCP"
        ocpcleanup dev
        echo "Cutting release branch for $gitBranch"
        git checkout master
        git add .
        git stash -m "Autostash"
        git pull
        git checkout -b release/$branchName
        git push
        echo "Release branch cut and pushed."
    else
        echo "Not a feature branch. Please migrate it to a feature/ branch."
    fi
}

cleanupRelease()
{
    gitBranch=`git rev-parse --abbrev-ref HEAD`
    branchName=`echo $gitBranch | cut -d "/" -f 2`
    if [[ "$gitBranch" == *"release"* ]]; then
    echo "Should cleanup all"
    ocpcleanup all
    else
    echo "Not a release branch. Use ocpcleanup 'env' if you know its been deployed"
    fi
}

getGitBranchRoute(){
    serviceName="${PWD##*/}"
    serviceName=`echo $serviceName | sed 's/dapi-//'`
    gitBranch=`git rev-parse --abbrev-ref HEAD`
    branchType=`echo $gitBranch | cut -d "/" -f 1`
    branchName=`echo $gitBranch | cut -d "/" -f 2`
    case $branchType in
      master | main)
        echo "No autocleanup of master/main allowed"
        return 1
        ;;

      feature)
        branchRoute="$serviceName-feature-$branchName"
        echo "Route for feature branch: $branchRoute"
        ;;

      release)
        branchRoute="$serviceName-release-$branchName"
        echo "Route for release branch: $branchRoute"
        ;;

      bugfix)
        branchRoute="$serviceName-bugfix-$branchName"
        echo "Route for bugfix branch: $branchRoute"
        ;;

      *)
        echo "Unrecognized branch type. Expects master, release/, bugfix/ or feature/"
        echo "Please double check your route name in OCP4"
        branchRoute="$serviceName-$gitBranch"
        echo "Route for unknown branch: $branchRoute"
        return 1
        ;;
    esac
    branchRoute=$(echo "$branchRoute" | tr '[:upper:]' '[:lower:]')
}

function ocpcleanup()
{
    currentDir=`pwd -W`
    case $1 in
        dev)
            envBranch=$devBranch
        ;;
        qa)
            echo "No QA purge until we confirm dev"
            #envBranch=$qaBranch
        ;;
        cap)
            echo "No CAP purge until we confirm dev"
            #envBranch=$cap1Branch
        ;;
        all)
            ocpcleanup dev
           # ocpcleanup qa
           # ocpcleanup cap
        ;;
        *)
        echo "Specify env: dev, qa, cap, all"
        return
        ;;
    esac
    if getGitBranchRoute; then
        cd $helmPurgeHome
        git checkout $envBranch
        git pull
        echo $branchRoute    > ./conf/deployment.conf
        git add ./conf/deployment.conf
        git commit -m "Purging $branchRoute from $1"
        git push
    fi
    cd $currentDir
}

