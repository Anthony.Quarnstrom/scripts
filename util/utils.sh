#!/bin/bash

javargs() {
    echo "-Dspring.profiles.active=local -Xms4096m -Xmx4096m -Dcom.sun.xml.bind.v2.runtime.JAXBContextImpl.fastBoot=true -Djdk.io.File.enableADS=true -Djdk.xml.xpathExprGrpLimit=0 -Djdk.xml.xpathExprOpLimit=0 -Djdk.xml.xpathTotalOpLimit=0"
}

localprops() {
  echo "Use something like the following for your application-local.properties:"
  echo "======================================================================="
  echo "fis.keystore=\${FIS_KEYSTORE}
  com.ally.dapi.dre.jms.username=\${COM_ALLY_DAPI_DRE_JMS_USERNAME:admin}
  com.ally.dapi.dre.jms.password=\${COM_ALLY_DAPI_DRE_JMS_PASSWORD:admin}
  server.ssl.key-store=\${TLS_KEYSTR}
  server.ssl.key-store-password=\${TLS_CREDS}
  server.ssl.key-password=\${TLS_CREDS}
  cashedge.keystore=\${CASHEDGE_KEYSTORE}
  cashedge.client.keystore=\${CASHEDGE_CLIENT_KEYSTORE}
  keycloak.enabled=false
  server.port=8443
  com.ally.dapi.dre.jms.broker.url=(tcp://10.47.70.230:31011?sslEnabled=true&verifyHost=false&keyStorePath=\${TLS_KEYSTR}&keyStorePassword=\${TLS_CREDS}&useTopologyForLoadBalancing=false,tcp://10.47.70.102:31011?sslEnabled=true&verifyHost=false&keyStorePath=\${TLS_KEYSTR}&keyStorePassword=\${TLS_CREDS}&useTopologyForLoadBalancing=false)?ha=true
  com.ally.dapi.fis.url=https://xes-uat2-ally.fisglobal.com:9443
  dapi.log.capture.maskdata=false"
  echo "======================================================================="
  echo "Note that some of the props/certs aren't used across all services, so feel free to remove for a service if they're unused"
}

help()
{
  echo "javargs - output java args string for running local dapi services"
  echo "zidp - Displays current ZID password"
  echo "set_zidp 'password' - Sets zidp password"
  echo "ochelp - Provides info on OC utility commands"
}

zidp()
{
    printf %s $ZID_P
}

set_zidp()
{
    printf %s "export ZID_P='$1'" > $BASH_ENV/.zidp
    source $BASH_ENV/.zidp
}

dapi_env()
{
  echo "ACM_KEY:"$ACM_KEY
  echo "ACM_SECRET":$ACM_SECRET
  echo "TLS_CREDS":$TLS_CREDS
  echo "SALESFORCE_ENTERPRISE_USERNAME":$SALESFORCE_ENTERPRISE_USERNAME
  echo "SALESFORCE_ENTERPRISE_PASSWORD":$SALESFORCE_ENTERPRISE_PASSWORD
  echo "JMS_USER":$JMS_USER
  echo "JMS_PASS":$JMS_PASS
  echo "CASHEDGE_KEYSTORE_PATH":$CASHEDGE_KEYSTORE_PATH
  echo "FIS_XPRESS_CERT_PATH":$FIS_XPRESS_CERT_PATH
  echo "CASHEDGE_CERT_PATH":$CASHEDGE_CERT_PATH
  echo "TLS_KEYSTORE_PATH":$TLS_KEYSTORE_PATH
  echo "TLS_TRUSTSTORE_PATH":$TLS_TRUSTSTORE_PATH
  }
  