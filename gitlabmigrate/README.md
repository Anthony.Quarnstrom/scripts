# gitsync script
### Utility to migrate from bitbucket to gitlab

# Important Note
If you're using this to sync changes from bitbucket to gitlab, you will
need to reach out to L3 (Najeeb is a good start.) to get them to temporarily disable
the gitlab pre-hook that prevents a user from pushing commits not made by your verified email.
This pre-hook will cause any pushes with changes from bitbucket fail if they were made by
anyone that isn't the person running the sync.

# Prerequisites
- This assumes you have SSH Access to Bitbucket and Windows credential manager managed gitlab access
  - See [here](https://confluence.int.ally.com/display/CT/Gitlab+Access+and+Setup) for info on how to set up gitlab access.

# Usage
- First, edit the script to add the path to your git repo of the global ocp4 helm charts
located [here](https://bitbucket.int.ally.com/projects/DAPI/repos/helm-charts/browse)
- Then you can customize a few things if desired, but you really only need to update
the helm chart repo variable.
- Add the script to your path.
- Take your terminal of choice and cd to the root directory of the repo you want to migrate.

#`gitsync sync`
This command will sync just the branch you currently have checked out.

# `gitsync syncAll`
Note: this command will check the most recent commit date to determine if you
even need to do any syncing. However, if you run the migrate command first, it will
register that commit as a new commit. So run this first.
This command syncs bitbucket and gitlab by:
1. Point your repo to bitbucket and pull the latest changes with `git fetch --all`
2. Sort through the branch references and compile a list of all unmerged branches.
3. Place these in a file (branchesToPush.txt by default) in the root directory, for the user to review, and prune any branches they wish to not push.
4. Pulls back in the branches from the file to pickup any changes.
5. Switches the repo the gitlab equivalent repo. 
6. Iterates through each branch and pushes it individually to gitlab (This avoids pushing local branches you wish to not push, unlike `git push --all origin`)
7. It then grabs both repos branch list again and diffs them to ensure they match.
8. Tidies up the generated file, and leaves your repo pointing at Gitlab

# `gitsync migrate`
This command preps a PR branch for migrating to gitlab standards described [here](https://confluence.int.ally.com/display/CT/DAPI-BitBucket+to+Gitlab+Migration):
1. Does some pre-validation, such as ensuring the helm chart repo is found, and that you're in a repo.
2. Ensures the repo is pointed at bitbucket, and pulls the latest master code
3. Creates a new branch from latest master
4. Updates the pom file (removes the 'dapi-' segment of the artifact id to avoid build problems).
5. Removes any jenkinsfiles
6. Creates a new config directory in src/main/resources/
7. Copy's the services environment specific application.properties files from the helm repo chart, appending -{env} to their name, into the config directory.
8. Moves the application.properties and application-local.properties (if present) into the config directory.
9. Adds the .gitlab-ci.yml (for gitlab deploys) into the root of the repo.
10. Bundles all of these files up into a commit for you, and after the user has a chance to review the changes, will commit the changes and push the branch to gitlab.
    - If at this point the user does not approve the changes, they can either leave the branch as is, or automatically rollback all the changes.
    
# Misc. Commands
- swap
  - Switches the repo from gitlab -> bitbucket, or bitbucket->gitlab
- repo
  - Tells you what you're currently pointed at.
    
# Notes/Issues
The script does it's best to check that what it needs is there. However, you will probably want to watch the log output just in case.

I have seen a strange error trying to autopush branches that are in strange states. However, if a branch is missed in the push
it should show up in the diff. Feel free to suggest or make any improvements.