#!/bin/bash
#Variables you MUST set:
#Set this to the path of your helm-charts directory
helmChartRepoDirectory="C:\Users\gz70cm\code\helm-charts"

#Variables you can customise if desired:
migrationBranchName="feature/gitlab-automigration"
branchStorageFilename="branchesToPush.txt"

#Variables used by the script, do not change.
#Gitlab currently doesn't allow SSH connections, so we use HTTPS.
#See https://confluence.int.ally.com/display/CT/Gitlab+Access+and+Setup for steps
#to setup windows credential manager.
migrateDate=$(date -d "Oct 14 2022" +"%Y%m%d") #Date the bitbucket migration originally occured
gitlabServiceUrl="https://gitlab.com/allyfinancial/consumer-commercial-banking-technology/deposits-api/services/"
gitlabGatewayUrl="https://gitlab.com/allyfinancial/consumer-commercial-banking-technology/deposits-api/gateway/"
gitlabLibraryUrl="https://gitlab.com/allyfinancial/consumer-commercial-banking-technology/deposits-api/libraries/"
bitbucketUrl="ssh://git@bitbucket.int.ally.com/dapi/"
currentUrl="Not set"
repoName="Not Set"
newRemote="Not Set"
migratingRepoDir=`pwd`
serviceName="${PWD##*/}"
serviceName=`echo $serviceName | sed 's/dapi-//'`
echo "gitsync operating on $serviceName"

#swap from bitbucket to gitlab, and vice versa. Mostly for manual use
swap()
{
  get_current_repo_url
  if [[ $currentUrl == *"bitbucket"* ]]; then
    echo "Swapping to GitLab"
    swap_to_gitlab
  elif [[ $currentUrl == *"gitlab"* ]]; then
    echo "Swapping to Bitbucket"
    swap_to_bitbucket
  else
    echo "Somethings wrong with your current url. Not doing anything."
    echo $currentUrl
  fi
}

#Returns what repo we're currently pointing at. Mostly for manual use
repo()
{
  get_current_repo_url
  echo $currentUrl
}

#Utility to get the current remote url, trimmed of git output, suitable to set origin
get_current_repo_url()
{
  currentUrl=`git config --get remote.origin.url`
  repoName=$(echo "$currentUrl" | awk -F '/' '{print $NF}')
}

#Changes remote origin to gitlab URL
swap_to_gitlab()
{
  get_current_repo_url
  if [[ "$serviceName" == *"-gateway"* ]] #If its something we expect to have environment specific configs
  then
      newRemote="$gitlabGatewayUrl$repoName"
  elif [[ "$serviceName" == *"component" ]];
  then
      newRemote="$gitlabLibraryUrl$repoName"
  else
      newRemote="$gitlabServiceUrl$repoName"
  fi
  echo "Setting repository origin to "$newRemote
  git remote set-url origin $newRemote
  currentUrl=`git config --get remote.origin.url`
  echo "Updated Url:" $currentUrl
}

#Changes remote origin to bitbucket URL
swap_to_bitbucket()
{
  get_current_repo_url
  newRemote="$bitbucketUrl$repoName"
  echo "Setting repository origin to "$newRemote
  git remote set-url origin $newRemote
  currentUrl=`git config --get remote.origin.url`
  echo "Updated Url:" $currentUrl
}

#Gets a list of all branches, and pushes them to $branchStorageFilename for user review
get_all_unmerged_branches()
{
  git checkout master
  git pull
  git fetch --all --prune
  branches=()
  for branch in $(git for-each-ref --no-merged master --format='%(refname)' refs/remotes refs/heads); do
    trimmedBranch=$(echo $branch | awk -F '/' '{print $(NF-1)"/"$NF}')
    trimmedBranch=$(echo $trimmedBranch | awk '{sub(/origin\//,""); print}')
    trimmedBranch=$(echo $trimmedBranch | awk '{sub(/heads\//,""); print}')
    branches+=($trimmedBranch)
  done
  unique_branches=($(echo "${branches[@]}" | tr ' ' '\n' | sort -u | tr '\n' ' '))
  echo "Branches to push:"
  printf "%s\n" "${unique_branches[@]}" | tee $branchStorageFilename
  branches=() #Clear the array for future read.
  echo "Please check $branchStorageFilename to verify all the branches to push."
  echo "If you wish to not sync a branch, simply delete the whole line from the file."
}

#Pulls in potentially modified branch list from $branchStorageFilename
read_in_branches()
{
  readarray -t branches < $branchStorageFilename
}

#Compares two files containing the branch list from both bitbucket and gitlab to assure all branches are pushed.
compare_branches_from_both_repos()
{
  sort --unique -o bbranches.txt{,}
  sort --unique -o glbranches.txt{,}

  DIFF=$(diff bbranches.txt glbranches.txt)
  if [ "$DIFF" == "" ]
  then
    echo "Gitlab and Bitbucket branch lists are identical."
  else
    echo "There are differences between the two repos. Please check the list below."
    echo "Branches existing only in Bitbucket:"
    DIFF=($DIFF) #Put into list so we can skip first line of Diff output
      for line in ${DIFF[@]:1}; do
        if [[ "$line" =~ ^(<|>) ]]; then
          printf ""
        elif [ "$line" == "---" ]; then
          echo "Branches existing only in Gitlab:"
        else
          echo $line
        fi
      done
  fi
}

#Pulls both Repos branch lists, trims them down, and puts them in a file for comparison
get_both_repo_branches()
{
  swap_to_bitbucket
  git fetch --all --prune
  bbranches=()
  for bbranch in $(git for-each-ref --no-merged master --format='%(refname)' refs/remotes refs/heads); do
    trimmedBranch=$(echo $bbranch | awk -F '/' '{print $(NF-1)"/"$NF}')
    bbranches+=($trimmedBranch)
  done
  printf "%s\n" "${bbranches[@]}" > bbranches.txt
  swap_to_gitlab
  git fetch --all --prune
  glbranches=()
  for branch in $(git for-each-ref --no-merged master --format='%(refname)' refs/remotes refs/heads); do
    trimmedBranch=$(echo $branch | awk -F '/' '{print $(NF-1)"/"$NF}')
    glbranches+=($trimmedBranch)
  done
  printf "%s\n" "${glbranches[@]}" > glbranches.txt
}

#Compares branch lists from both bitbucket and gitlab to verify the sync
verify_repo_sync()
{
get_both_repo_branches
compare_branches_from_both_repos
rm bbranches.txt glbranches.txt
}

#Pushes all branches from bitbucket to gitlab
push_branches()
{
  read_in_branches
  swap_to_gitlab
  #Iterate and push each branch to gitlab
  for branch in ${branches[@]}; do
    git checkout $branch
    git pull
    git push -u origin $branch:$branch
  done
  git checkout master
  #Now push tags (follow tags means we only push tags that point to something reachable)
  git push --follow-tags
  verify_repo_sync
  echo "Pushes complete. Your Repo is now pointing at Gitlab."
}

#Lets user edit/verify the branch list file created in the repo home directory
confirm_branch_selection()
{
  echo "Are the listed branches correct? (y/n)"
  read continue
  if [ "$continue" == "y" ]
  then
    echo "Continuing sync"
    push_branches
  else
    echo "Not Continuing sync. Your repo is still pointing at Bitbucket."
  fi
}

cleanup_file()
{
  rm $branchStorageFilename
}

copy_gitlab_ci()
{
  if [[ "$serviceName" == *"-svc"* ]]
  then
    copy_gitlab_service_ci
  elif [[ "$serviceName" == *"-consumer"* ]];
  then
    copy_gitlab_service_ci
  elif [[ "$serviceName" == *"-component"* ]];
  then
    copy_gitlab_component_ci
  elif [[ "$serviceName" == *"-gateway"* ]];
  then
    copy_gitlab_gateway_ci
  else
    echo "Unrecognized type, treating like a service. Please double check results."
    copy_gitlab_service_ci
  fi
}

#Copies the gitlab ci file (or breaks if not found)
copy_gitlab_service_ci()
{
  printf "include:
  - project: \"allyfinancial/consumer-commercial-banking-technology/deposits-api/templates/dapi-gitlab-templates\"
    ref: \"feature/service-template\"
    file: \"dapi-openshift-pipeline.yml\"" > $migratingRepoDir/.gitlab-ci.yml
}

copy_gitlab_gateway_ci()
{
  printf "include:
  - project: \"allyfinancial/consumer-commercial-banking-technology/deposits-api/templates/dapi-gitlab-templates\"
    ref: \"feature/gateway-template\"
    file: \"dapi-gateway-pipeline.yml\"" > $migratingRepoDir/.gitlab-ci.yml
}

copy_gitlab_component_ci()
{
printf "include:
         - project: \"allyfinancial/consumer-commercial-banking-technology/deposits-api/templates/dapi-gitlab-templates\"
           ref: \"feature/nexus-template\"
           file: \"dapi-nexus-pipeline.yml\"" > $migratingRepoDir/.gitlab-ci.yml
}

#Removes the dapi- from the artifact ID in the repos POM
update_pom()
{
  ogServiceArtifact=`grep -o -m 1 -E '<artifactId>dapi-.*(-svc|-consumer)' pom.xml`
  ogServiceArtifact=`echo -e $ogServiceArtifact`
  newServiceArtifact=`echo $ogServiceArtifact | sed 's/dapi-//'`
  echo "Replacing old artifact id $ogServiceArtifact"
  echo "Updating pom artifactId to $newServiceArtifact"
  sed -i "s/${ogServiceArtifact}/${newServiceArtifact}/" $migratingRepoDir/pom.xml
}

#Verifies we're in a git repo before we start
check_if_repo()
{
  if [[ `git rev-parse --is-inside-work-tree` == "true" ]]; then
    echo "Inside a repo, continuing"
  else
    echo "Not inside a repo, exiting"
    exit 1
  fi
}

#Creates the config dir to hold application properties
create_config_dir()
{
  cd $migratingRepoDir
  echo "creating config directory in $migratingRepoDir"
  mkdir -p src/main/resources/config/
}

#Pulls the latest helm charts from the global helm chart repo (you'll have to pull this yourself initially)
fetch_latest_application_props()
{
  cd $helmChartRepoDirectory
  echo "Now in $helmChartRepoDirectory, pulling latest helm charts."
  git fetch
  git checkout feature/dapi-6094-ocp4-global-config
  git pull
  echo "Latest Helm Charts pulled."
  cd $migratingRepoDir
}

#Copies all of the repos configs for each env, and adds the env to the end of the filename
copy_application_props_to_config()
{
  cd $helmChartRepoDirectory
  echo "Copying environment service application props for $serviceName"
  serviceEnvPath="$helmChartRepoDirectory/services/$serviceName/env/"
  helmPropertiesFiles=$(find "$serviceEnvPath" -name *.properties)
  mapfile -t helmPropFiles <<< ${helmPropertiesFiles}
  for helmPropsFile in "${helmPropFiles[@]}"
  do
    IFS=/ read -a helmPath <<< "$helmPropsFile"
    helmEnv="${helmPath[${#helmPath[@]} - 2]}"
    echo "Copying $helmPropsFile as application-$helmEnv.properties"
    cp $helmPropsFile $migratingRepoDir/src/main/resources/config/application-$helmEnv.properties
  done
  cd $migratingRepoDir
  mv src/main/resources/application.properties src/main/resources/config/application.properties
  git rm src/main/resources/application.properties  #Quietly attempt to move application local, since I don't think they exist on 'most' of our services.
  mv src/main/resources/application-local.properties src/main/resources/config/application-local.properties 2>/dev/null
  git rm src/main/resources/application-local.properties 2>/dev/null
}

#Removes jenkinsfiles and .helm dir
delete_old_files()
{
  rm *Jenkinsfile*
  rm -rf .helm/ 2>/dev/null #Silent since we don't have this for non-svc repos
}

#Checks to make sure your $helmChartRepoDirectory is set correctly
check_if_helm_charts_exist()
{
if [ -d "$helmChartRepoDirectory" ];
 then
   echo "$helmChartRepoDirectory exists. Continuing."
 else
   echo "$helmChartRepoDirectory doesn't exist. Please correct the variable in the script."
   exit 1;
fi
}

#Creates a pr branch for the gitlab migration stuff
create_pr_branch()
{
  cd $migratingRepoDir
  git checkout master
  git pull
  if [ `git branch --list $migrationBranchName` ]
  then
    git checkout $migrationBranchName
  else
    git checkout -b $migrationBranchName
  fi
}

#Adds only gitlab migration steps to the PR we've created
add_changes_to_pr()
{
  git add pom.xml
  git add .gitlab-ci.yml
  git add src/main/resources/config/
  git add .helm/
  git add Jenkinsfile*
}

#Gives the user a chance to eyeball all of the changes for correctness. Allows for rollback if there's issues
verify_before_commit()
{
  echo "Please verify the changes to ensure everything is looking good. Would you like to commit? (y/n)"
  read continue
  if [ "$continue" == "y" ]
  then
    echo "Committing changes"
    git commit -m "Migrates config/deploy to gitlab model."
    echo "Feel free to touch up the commit message with 'git commit --amend' if you would like."
    echo "Would you like to push to gitlab? (y/n)"
    read push
    if [ "$push" == "y" ]
      then
        echo "Pushing to gitlab."
        swap_to_gitlab
        git push -u origin $migrationBranchName
      else
        echo "Exiting"
      fi
  else
    echo "Not committing changes. Would you like to rollback? (y/n)"
    read rollback
    if [ "$rollback" == "y" ]
      then
        echo "rolling back to master and deleting current change branch. Re-run script to recreate."
        git commit -m "delete"
        git checkout master
        git branch -D $migrationBranchName
        git restore src/main/resources/application.properties
        #Quietly try to restore application-local, since it doesn't exist on most of our repos.
        git restore src/main/resources/application-local.properties 2>/dev/null
      else
        echo "Exiting"
      fi
  fi
}

migrate_properties()
{
  create_config_dir
  fetch_latest_application_props
  copy_application_props_to_config
}

##Takes steps to migrate the repo to the new gitlab format.
migrate()
{
  check_if_repo
  check_if_helm_charts_exist
  swap_to_bitbucket
  create_pr_branch
  update_pom
  copy_gitlab_ci
  if [[ "$serviceName" == *"-svc"* ]] #If its something we expect to have environment specific configs
  then
    migrate_properties
  fi
  if [[ "$serviceName" == *"-consumer"* ]] #If its something we expect to have environment specific configs
  then
    migrate_properties
  fi
  delete_old_files
  add_changes_to_pr
  verify_before_commit
}

check_if_any_changes_after_migration()
{
  git fetch --all --prune
  recentDate=`git for-each-ref --sort=-committerdate refs/heads/ --format='%(committerdate)' | awk 'NR==1{print $2" "$3" "$5}'`
  recentDate=$(date -d "$recentDate" +"%Y%m%d")
  if [[ $recentDate > $migrateDate ]];
  then
    echo "most recent commit is after migrate date, proceeding with sync."
      get_all_unmerged_branches
      confirm_branch_selection
      cleanup_file
  else
    echo "Most recent date is before migrate date. Nothing to sync."
  fi
}
#Sync from bitbucket to gitlab, with verification
syncAll()
{
  check_if_repo
  swap_to_bitbucket ##Make sure we're pulling from bitbucket
  check_if_any_changes_after_migration
  echo "Exiting script"
}

sync()
{
  check_if_repo
  swap_to_bitbucket
  git pull
  swap_to_gitlab
  git push
}

help()
{
  if [[ $1 == "swap" ]]; then
    echo "Swap will switch your repo from bitbucket to gitlab, and vice versa."
  elif [[ $1 == "sync" ]]; then
    echo "sync will sync just the current branch you're on."
  elif [[ $1 == "syncAll" ]]; then
    echo "SyncAll will sync the entire repo from bitbucket to gitlab."
  elif [[ $1 == "repo" ]]; then
    echo "repo will tell you where you're currently pointing."
  elif [[ $1 == "migrate" ]]; then
    echo "Will setup all the changes for the gitlab migration"
  else
    echo "This script helps with repo migration."
    echo "Just put this script in your path, and run from the root of your repo."
    echo "If you want to fully sync a repo, just run ./gitlab.sh sync"
    echo "Here are our possible commands (use help <command> to learn more):"
    printf "help\nswap\nsync\nsyncAll\nrepo\nmigrate"
  fi
}

if [ $# -eq 0 ]; then
      help
      exit 0
fi
#Iterate through args
for i in "$@"; do
  if [[ $1 == "help" ]]; then
    help $2
    break
  else
    "$i"
  fi
done