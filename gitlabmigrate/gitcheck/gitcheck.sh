#!/bin/bash

gitlabBaseURL="https://gitlab.com/allyfinancial/consumer-commercial-banking-technology/deposits-api/services"
bitbucketBaseURL="ssh://git@bitbucket.int.ally.com/dapi"
home=$(pwd -W)

init() {
    mapfile -t repos < ./gitlab/services.repos.txt

    for repo in "${repos[@]}"
    do
        repo=$(echo $repo | sed -r 's/\s+//g') #remove whitespace thats causing issues
        shortRepo=$(echo $repo | sed -e 's/dapi-//g')
        echo "Working on $repo"
        echo "Cloning GL $shortRepo"
        git clone $gitlabBaseURL/$repo.git ./gitlab/$shortRepo

        echo "Cloning BB $shortRepo"
        git clone $bitbucketBaseURL/$repo.git ./bitbucket/$shortRepo
    done
}

check() {
    mapfile -t repos < ./gitlab/services.repos.txt
    rm comparison/tags/*
    rm comparison/commits/*
    rm $home/comparison/summary/*.txt
    for repo in "${repos[@]}"
    do
        repo=$(echo $repo | sed -r 's/\s+//g') #remove whitespace thats causing issues
        shortRepo=$(echo $repo | sed -e 's/dapi-//g')
        echo "Checking $shortRepo"
        cd gitlab/$shortRepo
        touch $shortRepo.commits.txt
        git log --format=format:"%H" > $shortRepo.commits.txt
        git for-each-ref --format="%(refname:short)" refs/tags > $shortRepo.tags.txt
        cd $home
        cd bitbucket/$shortRepo
        touch $shortRepo.commits.txt
        git log --format=format:"%H" > $shortRepo.commits.txt
        git for-each-ref --format="%(refname:short)" refs/tags > $shortRepo.tags.txt
        cd $home
        diff bitbucket/$shortRepo/$shortRepo.commits.txt ./gitlab/$shortRepo/$shortRepo.commits.txt > ./comparison/commits/$shortRepo.commits.txt
        diff bitbucket/$shortRepo/$shortRepo.tags.txt ./gitlab/$shortRepo/$shortRepo.tags.txt > ./comparison/tags/$shortRepo.tags.txt
        gawk -i inplace '!/>/' ./comparison/commits/$shortRepo.commits.txt
        gawk -i inplace '!/>/' ./comparison/tags/$shortRepo.tags.txt
        sed -i '1d;$d' ./comparison/commits/$shortRepo.commits.txt
        sed -i '1d;$d' ./comparison/tags/$shortRepo.tags.txt
        sed -i 's/<//g' ./comparison/tags/$shortRepo.tags.txt
        sed -i 's/<//g' ./comparison/commits/$shortRepo.commits.txt
        sed -i 's/-//g' ./comparison/tags/$shortRepo.tags.txt
        sed -i 's/-//g' ./comparison/commits/$shortRepo.commits.txt
        gawk -i inplace '!length($0)<39' ./comparison/commits/$shortRepo.commits.txt
        gawk -i inplace '!/,/' ./comparison/tags/$shortRepo.tags.txt
        gawk -i inplace '!/,/' ./comparison/commits/$shortRepo.commits.txt

        if [ -s ./comparison/commits/$shortRepo.commits.txt ]; then
                # The file is not-empty
                echo "Differences for $repo commits"
        else
                # The file is empty.
                rm ./comparison/commits/$shortRepo.commits.txt
        fi
        if [ -s ./comparison/tags/$shortRepo.tags.txt ]; then
                # The file is not-empty
                echo "Differences for $repo tags"
        else
                # The file is empty.
                rm ./comparison/tags/$shortRepo.tags.txt
        fi
        details $shortRepo
    done
}

details()
{
    mapfile -t commits < ./comparison/commits/$1.commits.txt
    mapfile -t tags < ./comparison/tags/$1.tags.txt
    cd bitbucket/$shortRepo
    for commit in "${commits[@]}"
    do
        echo "git show $commit >> $home/comparison/summary/$1.commits.txt"
        git show $commit >> $home/comparison/summary/$1.commits.txt
    done
    for tag in "${tags[@]}"
    do
        echo "git show $tag >> $home/comparison/summary/$1.tags.txt"
        git show $tag >> $home/comparison/summary/$1.tags.txt
    done
    cd $home
}

init
check