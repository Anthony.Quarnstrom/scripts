# localcerts script
### Utility to grab latest DEV certs and creds for local services

# Prerequisites
- This assumes you have OCP4 access.

# Usage
- Alter the path where you want to store your certs. I choose a central location, rather than in individual services.
- Get your OC login command by logging into ocp4 and clicking get token in the top right menu.
- Place the login command at the top of the script replacing the comment.
- Run the script (I.e. ./localcerts getAll ).
- Ensure the service either has the correct env variable names, or translate them in application-local.properties like below:
    
        cashedge.client.keystore=${CASHEDGE_CLIENT_KEYSTORE}
        cashedge.keystore=${CASHEDGE_KEYSTORE}
        TLS_KEYSTORE=${TLS_KEYSTR}
        TLS_TRUSTSTORE=${TLS_TRUSTSTR}
        TLS_PASSWORD=${TLS_CREDS}
        fis.keystore=${FIS_KEYSTORE}
        
        com.ally.dapi.dre.jms.username=${COM_ALLY_DAPI_DRE_JMS_USERNAME}
        com.ally.dapi.dre.jms.password=${COM_ALLY_DAPI_DRE_JMS_PASSWORD}

# getAll
Get's both certs, and the passwords for the TLS certs, since that one changes.

# getCerts
Get's just certs and puts them in the folder specified.

# setCreds
Gets the TLS cert creds, and sets them to a windows env variable for your usage. Or you can just grab it and put it in your application.conf.

## TODO
- Automate the OC login process

#Environment Variables Set by this script:
 - ACM_APIGEE_ACCESS_KEY
    - The apigee access key for ACM v6
 - ACM_APIGEE_SECRET_KEY
    - The apigee secret key for ACM v6
 - CASHEDGE_CLIENT_KEYSTORE
    - path to payment-identity.jks
 - CASHEDGE_KEYSTORE
    - path to cashedge.jks
 - TLS_KEYSTR
    - path to keystore.jks
 - TLS_TRUSTSTR
    - path to truststore.jks
 - TLS_CREDS
    - the password to the tls stores
 - FIS_KEYSTORE
    - path to fis-xpress.jks
 - COM_ALLY_DAPI_DRE_JMS_USERNAME
    - the JMS username for dev
 - COM_ALLY_DAPI_DRE_JMS_PASSWORD
    - the JMS password for dev