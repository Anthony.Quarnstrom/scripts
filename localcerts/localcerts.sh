#!/bin/bash -l
LOCAL_CERT_PATH="C:\Users\gz70cm\code\local\certs"
#Set your ZID and ZID password as env variables (ZID and ZID_P respectively) to auto-login
#Or if you set them elsewhere, you can add a -l to the #/bin/bash to pass your local env, so you dont have to
#have them written down here.

#ZID=user
#ZID_P=pass

login() {
  #To use the login utility, make sure you have the ocutil.sh script sourced in your bashrc
  if [[ $(type -t oclogin) == function ]]; then
    echo "logging in with utility"
    if [ -n "${ZID}" ] && [ -n  "${ZID_P}" ]; then
          oclogin dev
      else
          ocloginprompt dev
    fi
  else
    echo "logging in with token"
  #place oc login command here
  #Something like:
  #oc login --token=sha256~c9dlYR6fBfz1MDrulJO4HQ8mkBFMEyJKZyDVgLhHWXY --server=https://api.sat-ocp-dev.int.ally.com:6443
  fi
}

getCertsLoggedIn() {
  POD_NAME=$(oc get pods -o custom-columns=POD:.metadata.name --no-headers | grep customer-svc-master | tr -d SET1)
  echo "Downloading certs from $POD_NAME"
  echo "Downloading fis-xpress.jks"
  oc cp $POD_NAME:/etc/keystores/..data/fis-xpress.jks ./fis-xpress.jks
  echo "Downloading TLS Keystore"
  oc cp $POD_NAME:/run/secrets/java.io/keystores/keystore.jks ./keystore.jks
  echo "Downloading TLS Truststore"
  oc cp $POD_NAME:/run/secrets/java.io/keystores/truststore.jks ./truststore.jks
  echo "Downloading cashedge payment-identity.jks"
  oc cp $POD_NAME:/etc/keystores/cashedge/client/..data/payment-identity.jks ./payment-identity.jks
  echo "Downloading cashedge.jks"
  oc cp $POD_NAME:/etc/keystores/cashedge/..data/cashedge.jks ./cashedge.jks

  echo "Moving certs to $LOCAL_CERT_PATH"
  mv fis-xpress.jks $LOCAL_CERT_PATH
  mv keystore.jks $LOCAL_CERT_PATH
  mv truststore.jks $LOCAL_CERT_PATH
  mv payment-identity.jks $LOCAL_CERT_PATH
  mv cashedge.jks $LOCAL_CERT_PATH
}

setPaths() {
  CERT_BASE_PATH="C:/Users/gz70cm/code/local/certs/"
  FIS_XPRESS_CERT="fis-xpress.jks"
  TLS_KEYSTORE="keystore.jks"
  TLS_TRUSTSTORE="truststore.jks"
  CASHEDGE_KEYSTORE="payment-identity.jks"
  CASHEDGE_CERT="cashedge.jks"

  FIS_XPRESS_CERT_PATH=$CERT_BASE_PATH$FIS_XPRESS_CERT
  TLS_KEYSTORE_PATH=$CERT_BASE_PATH$TLS_KEYSTORE
  TLS_TRUSTSTORE_PATH=$CERT_BASE_PATH$TLS_TRUSTSTORE
  CASHEDGE_KEYSTORE_PATH=$CERT_BASE_PATH$CASHEDGE_KEYSTORE
  CASHEDGE_CERT_PATH=$CERT_BASE_PATH$CASHEDGE_CERT

  setx CASHEDGE_KEYSTORE $CASHEDGE_KEYSTORE_PATH
  setx FIS_KEYSTORE $FIS_XPRESS_CERT_PATH
  setx CASHEDGE_CLIENT_KEYSTORE $CASHEDGE_CERT_PATH
  setx TLS_KEYSTR $TLS_KEYSTORE_PATH
  setx TLS_KEYSTORE $TLS_KEYSTORE_PATH
  setx TLS_TRUSTSTR $TLS_TRUSTSTORE_PATH
   setx TLS_TRUSTSTORE $TLS_TRUSTSTORE_PATH
}

setJMS() {
  echo "Getting JMS username and password"
  JMS_PASS=$(oc get secret amq75-client -o go-template --template="{{.data.password|base64decode}}")
  JMS_USER=$(oc get secret amq75-client -o go-template --template="{{.data.username|base64decode}}")
  setx COM_ALLY_DAPI_DRE_JMS_USERNAME $JMS_USER
  setx COM_ALLY_DAPI_DRE_JMS_PASSWORD $JMS_PASS
  setx COM_ALLY_DAPI_AMQ_USERNAME $JMS_USER
  setx COM_ALLY_DAPI_AMQ_PASSWORD $JMS_PASS
}

setSalesforce()
{
  echo "Getting Salesforce username and password"
  SALESFORCE_ENTERPRISE_USERNAME=$(oc get secret salesforce -o jsonpath="{.data['salesforce\.userName']}" | base64 -d)
  SALESFORCE_ENTERPRISE_PASSWORD=$(oc get secret salesforce -o jsonpath="{.data['salesforce\.password']}" | base64 -d)
  echo $SALESFORCE_ENTERPRISE_USERNAME
  echo $SALESFORCE_ENTERPRISE_PASSWORD
  setx SALESFORCE_ENTERPRISE_USERNAME $SALESFORCE_ENTERPRISE_USERNAME
  setx SALESFORCE_ENTERPRISE_PASSWORD $SALESFORCE_ENTERPRISE_PASSWORD
}

setTLS() {
  echo "Getting TLS truststore password"
  TLS_CREDS=$(oc get secret customer-svc-master-tls-creds -o go-template --template="{{.data.password|base64decode}}")
  setx TLS_CREDS $TLS_CREDS
  setx TLS_PASSWORD $TLS_CREDS
}

setACM()
{
  echo "Getting ACM Creds"
  ACM_KEY=$(oc get secret dapi-acm-apigee-secret -o jsonpath="{.data['access\.key']}" | base64 -d)
  ACM_SECRET=$(oc get secret dapi-acm-apigee-secret -o jsonpath="{.data['secret\.key']}" | base64 -d)
  setx ACM_APIGEE_PRODUCT_KEY $ACM_KEY
  setx ACM_APIGEE_SECRET_KEY $ACM_SECRET

}

localProps() {
  echo "Use something like the following for your application-local.properties:"
  echo "======================================================================="
  echo "fis.keystore=\${FIS_KEYSTORE}
  com.ally.dapi.dre.jms.username=\${COM_ALLY_DAPI_DRE_JMS_USERNAME:admin}
  com.ally.dapi.dre.jms.password=\${COM_ALLY_DAPI_DRE_JMS_PASSWORD:admin}
  server.ssl.key-store=\${TLS_KEYSTR}
  server.ssl.key-store-password=\${TLS_CREDS}
  server.ssl.key-password=\${TLS_CREDS}
  cashedge.keystore=\${CASHEDGE_KEYSTORE}
  cashedge.client.keystore=\${CASHEDGE_CLIENT_KEYSTORE}
  keycloak.enabled=false
  server.port=8443
  com.ally.dapi.dre.jms.broker.url=(tcp://10.47.70.230:31011?sslEnabled=true&verifyHost=false&keyStorePath=\${TLS_KEYSTR}&keyStorePassword=\${TLS_CREDS}&useTopologyForLoadBalancing=false,tcp://10.47.70.102:31011?sslEnabled=true&verifyHost=false&keyStorePath=\${TLS_KEYSTR}&keyStorePassword=\${TLS_CREDS}&useTopologyForLoadBalancing=false)?ha=true
  com.ally.dapi.fis.url=https://xes-uat2-ally.fisglobal.com:9443
  dapi.log.capture.maskdata=false"
  echo "======================================================================="
  echo "Note that some of the props/certs aren't used across all services, so feel free to remove for a service if they're unused"
}

setCredsLoggedIn() {
  echo "Getting dev credentials"
  setTLS
  setJMS
  setACM
}

setCreds() {
  login
  setCredsLoggedIn
}

getCerts() {
  login
  getCertsLoggedIn
  setPaths
}

getAll() {
  login
  getCertsLoggedIn
  setPaths
  setCredsLoggedIn
  setSalesforce
  localProps
}

help() {
  if [[ $1 == "getAll" ]]; then
    echo "Will login and run getCerts and setCreds for you."
  elif [[ $1 == "getCerts" ]]; then
    echo "Will login and pull the lastest of all of our certs needed locally."
  elif [[ $1 == "setCreds" ]]; then
    echo "Will pull latest cert creds and set them as a windows env variable"
    echo "You can also get individual creds with setTLS or setJMS."
  elif [[ $1 == "localProps" ]]; then
    echo "Outputs a local property file, useable generally in all of our services when locally running"
  else
    echo "This script helps with grabbing the latest certs."
    echo "You simply need to get your OC login token, and place it at the top of the file in the proper variable"
    echo "Here are our possible commands (use help <command> to learn more):"
    printf "help\ngetAll\ngetCerts\nsetCreds"
  fi
}

if [ $# -eq 0 ]; then
  help
  exit 0
fi
#Iterate through args
for i in "$@"; do
  if [[ $1 == "help" ]]; then
    help $2
    break
  else
    "$i"
  fi
  echo "You will have to completely kill intellij (And all jetbrains IDE's) to pick up the changed env variables".
done
