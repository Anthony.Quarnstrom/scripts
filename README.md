# Scripts
If something isn't listed in this README, it isn't ready for general consumption (though feel free to mess with it if that's your thing!)

# DAPI Bookmarks
Inspired by Stuart, a well named and organized collection of our Gitlab repo bookmarks. Simply to ease repo access.

# gitlabmigrate
No longer needed (we've migrated), but preserved in case someone else needs it.

# localcerts
Utility script to log you in to DEV OCP4 and pull all of the secrets and certs we need to run everything locally,
as well as an example application-local.properties config file to reference the set environment variables.

# routes
A utility to auto-generate a route change file for the gitlab route change utility, based on your current repo branch.

# util
Some general utilities (like a suite of OC command line utils courtesy of Stuart). Not necessary, but you may find them useful.
Some of these may be duplicated into other code (notable the oclogin stuff into the localcerts script) so that localcerts
gets a more convenient login without making people import anything else.

# Questions or Issues
Let me know or feel free to push an MR, always welcome contributions.