# Routes script
### Utility to generate route cut-over files for the gitlab utility pipelines

# Prerequisites
- This assumes you have access to the 
[dev](https://gitlab.com/allyfinancial/consumer-commercial-banking-technology/deposits-api/tools/route-switch/-/blob/release/dev/conf/dev_routesflip.conf)
[qa](https://gitlab.com/allyfinancial/consumer-commercial-banking-technology/deposits-api/tools/route-switch/-/blob/release/qa/conf/qa_routesflip.conf)
and [cap](https://gitlab.com/allyfinancial/consumer-commercial-banking-technology/deposits-api/tools/route-switch/-/blob/release/cap1/conf/cap1_routesflip.conf)
route flip utilities.
- You must also copy the routes and ENVroutes.conf files onto your path.
- Go into the script and edit your routeFilePath:
```routeFilePath="C:/Users/gz70cm/bin/"```
- Pull the routes repo: https://gitlab.com/allyfinancial/consumer-commercial-banking-technology/deposits-api/tools/route-switch
- Set the routeSwitchHome in the script to wherever you put that repo, i.e.:
```routeSwitchHome="C:/Users/gz70cm/code/DAPI/tools/route-switch"```

# Usage
- Edit the routes file (after placing on your path) so that the routeFilePath variable has the full path to your ENVroutes.conf files
- Then simply open up your service repo of choice, ensure you have the branch you've deployed checked out, then run

    routes ENV
    #Where env can be any value of dev1, dev2, dev3, qa1, qa2, qa3, cap1, cap2
    #Or run routes help to receive help.

- If you're in your services git repo with the proper branch checked out, you'll receive file output like:


    $ routes qa1
    
    Generating routes for qa1
    
    Getting routes for beneficiary-svc
    
    routes found: ext-ENV-beneficiary-svc ENV-beneficiary-svc ENV-beneficiary-svc-health
    
    Route for master branch: beneficiary-svc-master
    
    Route Change File
    ====================================================
    
    ext-qa1-beneficiary-svc,beneficiary-svc-master
    qa1-beneficiary-svc,beneficiary-svc-master
    qa1-beneficiary-svc-health,beneficiary-svc-master
    
    ====================================================

- Double-check your route output
- Enter 'yes' at the prompt to automatically push your changes on the proper route of the route change tool
- If there's any errors at this step, simply check lines 108-116 to see the basic git steps that are happening behind the scenes.
- The most common error is if you've left uncommitted changes in the routes repo (which you generally shouldn't have to).

# In Progresss

# Known Issues

- Fixed Issues with the name collision, however, there MAY be certain weird route names that may not be picked up by the regex.
- If you notice missing routes (the routes.conf file was created from QA1's service routes), simply add the missing route name
to the routes.conf file, and it will automatically get picked up. Also please MR it, so everyone can have the update!