echo "Bash RC version 2.0.0"
export GIT_SSH_COMMAND="ssh -i /c/users/gz70cm/.ssh/ally_id_rsa"
export CA_BUNDLE=/c/users/${USERNAME}/.certs/ca-cert.crt
export AWS_SHARED_CREDENTIALS_FILE=/c/users/${USERNAME}/.aws/credentials
export AWS_PROFILE=saml
export REQUESTS_CA_BUNDLE=$CA_BUNDLE
export SSL_CERT_FILE=$CA_BUNDLE
export AWS_CA_BUNDLE=$CA_BUNDLE
export PIP_CONFIG_FILE=~/.pip/pip.conf
export PIP_CONFIG_FILE=~/.pip/pip.conf
export PATH="/C/Program Files/Microsoft VS Code/bin:$PATH"
export PATH="~/bin:$PATH"
export PATH="~/OneDrive - Ally Financial/apps:$PATH"
###To enable python path remove the # and replace pythonVersion with the correct verison on your workstation###
export PATH="/C/Program\ Files/Python310:$PATH"

function set-git-global(){
	echo "Setting Git global requirements"
	git config --global credential.helper wincred
	git config --global http.sslVerify false
}
function delete-gitlab-winicred() {
  echo "Removing stored git windows credentials"
  pwsh ~/psScript.ps1 $password
}
function delete-aws-cred(){
  rm -rf ~\.aws\* -f
}
function awslogin() {
  export saml_username=nao\\x_${USERNAME}
  read -s -p "Password for $saml_username: " password
  export saml_password=$password
  aws-login --duration 28800 
}
function rawurlencode() {
  local string="${1}"
  local strlen=${#string}
  local encoded=""
  local pos c o

  for (( pos=0 ; pos<strlen ; pos++ )); do
     c=${string:$pos:1}
     case "$c" in
        [-_.~a-zA-Z0-9] ) o="${c}" ;;
        * )               printf -v o '%%%02x' "'$c"
     esac
     encoded+="${o}"
  done
  echo "${encoded}"
}

function proxy() {
  local password
  read -s -p "Proxy password for ${USERNAME}: " password
  echo ""
  echo "Checking network..."
  # Determine which proxy to use (Default DFW)
  local dfw_proxy_ip="10.43.196.154"
  local sat_proxy_ip="10.47.196.154"
  local sat_subnet="10.44.112.0/24"
  local proxy_ip=$dfw_proxy_ip
  if [[ $(in_subnet $sat_subnet $(nslookup-ip $(hostname))) == 1 ]]; then
    proxy_ip=$sat_proxy_ip
    echo "Proxy set to SAT (${sat_proxy_ip})"
  else
    echo "Proxy set to DFW (${dfw_proxy_ip})"
  fi
 
  export HTTP_PROXY=http://${USERNAME}:$(rawurlencode "$password")@${proxy_ip}:80
  export HTTPS_PROXY=$HTTP_PROXY
  git config --global --unset-all http.proxy
  git config --global --unset-all https.proxy
  git config --global --add http.proxy $HTTP_PROXY
  git config --global --add https.proxy $HTTP_PROXY
}

function unset-proxy() {
  unset HTTP_PROXY
  unset HTTPS_PROXY
  unset NO_PROXY
  git config --global --unset-all http.proxy
  git config --global --unset-all https.proxy

}

#Change path to this repo
HOME=$(pwd -W | cut -d'/' -f1-3)
for f in $HOME/code/scripts/util/*.sh; do echo "sourcing $f"; source $f; done
#source $HOME/code/scripts/util/*.sh
source $HOME/code/scripts/oc/*.sh
BASH_ENV="$HOME/code/scripts/bash/bashenv"
 for f in $BASH_ENV/.*; do source $f; done

set-git-global

export PATH="/c/users/gz70cm/bin:$PATH"
