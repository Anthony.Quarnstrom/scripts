#!/bin/bash
#All credit to Stuart for these
#You are expected to have already logged in to OC
#Recommend you source this in your .bashrc for use in other scripts (if you #!/bin/bash -l in those scripts)

function ochelp() {
        echo "ocroutecheck {service}"
        echo "ocfindservices {service}"
        echo "ocfindroutesforservice {service} {release}"
        echo "ocreleaseroute {env} {service}"
        echo "octag {service} ({release})"
        echo "Usage: oclogin {ENV}"
        echo "oclogin {ENV}: dev qa cap1 cap2 psp1 psp2 prod1 prod2"
}

function ocdeploys() {
    oc get deployments | awk ' { printf "%-55s %s\n", $1, $5 }' | grep -v -E '*-master|*-dev*|*-qa*' | sort -k 1 | grep -E "account|check|customer|correspondence|disclosure"
}

function ocroutecheck() {
        oc get routes --selector app=$1 | awk ' { printf "%-55s %s\n", $1, $4 }'
}

function ocfindservices() {
        oc get deployments --selector app.kubernetes.io/name=$1 | awk '{ print $1; }'
}

function ocfindroutesforservice() {
        oc get routes --selector app=$1 | grep $1-$2 | awk '{ print $1; }'
}

function ocreleaseroute() {
        oc get routes --selector release=static-routes,tm_env=$1 | grep $2 | awk '{ printf "%-55s %s\n", $1, $4 }'
}

function octag() {
        deploymentName="$1"
        if [ -n "$2" ]
        then
                deploymentName="$deploymentName-$2"
        fi
        tag=$(oc get deployment --selector app.kubernetes.io/instance=$deploymentName --output=yaml | grep image_tag | awk '{ sub(/^[ \t]+/, ""); print }' | awk '{ sub(/image_tag/,""); print }')

        echo "$deploymentName tag: $tag"
}

export OCPDEV="https://api.sat-ocp-dev.int.ally.com:6443"
export OCPQA="https://api.sat-ocp-qa.int.ally.com:6443"
export OCPCAP1="https://api.sat-ocp-cap1.int.ally.com:6443"
export OCPCAP2="https://api.sat-ocp-cap2.int.ally.com:6443"
export OCPPSP1="https://api.sat-ocp-psp1.int.ally.com:6443"
export OCPPSP2="https://api.sat-ocp-psp2.int.ally.com:6443"
export OCPPROD1="https://api.dfw-ocp-prod1.int.ally.com:6443"
export OCPPROD2="https://api.dfw-ocp-prod2.int.ally.com:6443"

function ochelp()
{
    ocwhat
}

function validateEnv()
{
case $1 in
  dev)
    loginEnv="OCPDEV"
    return 0
    ;;

   qa)
    loginEnv="OCPQA"
    return 0
    ;;

   cap1)
    loginEnv="OCP"$(echo "$1" | tr '[:lower:]' '[:upper:]')
    return 0
    ;;

    psp1 | psp2)
    loginEnv="OCP"$(echo "$1" | tr '[:lower:]' '[:upper:]')
    return 0
    ;;

    prod1 | prod2)
    loginEnv="OCP"$(echo "$1" | tr '[:lower:]' '[:upper:]')
    return 0
    ;;

  ochelp)
    ochelp
    return 1
    ;;

  *)
    echo "Unrecognized Environment param."
    ochelp
    return 1
    ;;
esac
}

function oclogin() {
        if validateEnv $1; then
            echo "Logging into ${!loginEnv}"
            url=${!loginEnv}
            oc login $url -u $ZID -p $(zidp)
        fi
}

function ocloginprompt()
{
    if validateEnv $1; then
            echo "Logging into ${!loginEnv}"
            url=${!loginEnv}
            oc login $url
    fi
}